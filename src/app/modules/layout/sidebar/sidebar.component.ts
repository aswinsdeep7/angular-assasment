import { Component, OnInit } from '@angular/core';
import { COUNTRIES } from 'src/app/config/sample.config';
import { CountryService } from 'src/app/service/country.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  selectedCountry: any = {}
  countries: any = COUNTRIES;
  constructor(private countrySer: CountryService) { }

  onClickCountry(data: any) {
    this.countrySer.setselectedCountry(data);
    this.selectedCountry = data.code;
  }

}
