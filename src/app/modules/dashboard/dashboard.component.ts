import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/service/country.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  selectedCountry: any;
  cityList: any;
  cityWithTemp: any = [];
  currentIndex = 0;
  cityObj: any = {};
  isLoading: any = false;
  index:number = 0;
  itemsPerpage:number = 4;
  constructor(private cs: CountryService) {

  }

  ngOnInit(): void {
    this.cs.selectedCountry$.subscribe(res => {
      if (res) {
        this.getCityListByCountryCode(res);
      }
      this.selectedCountry = res;
    })
  }

  getCityListByCountryCode(country: any): void {
    this.cityWithTemp=[]
    this.cs.getCityByCountryCode(country)
      .subscribe(async (data: any) => {
        this.cityList = data;
        this.getTempByCity(this.cityList);
        let count = 0;
      });
  }

  async getTempByCity(cityList: any) {
    for(const item of cityList.slice(this.index,this.itemsPerpage)) {
      const result:any = await this.getTemOfCity(item);
      this.cityWithTemp.push({ ...item, temp: JSON.parse(result? result:null) })
    };
  }

  loadMore(){
    this.index = this.itemsPerpage;
    this.itemsPerpage += 4;
    this.getTempByCity(this.cityList);
  }
  pageChange(correntIndex: number) {
    this.cityWithTemp = []
    for (let i = correntIndex * 4; i < (correntIndex * 4) + 4; i++) {
      this.getTemOfCity(this.cityList[i])
    }
  }

  async getTemOfCity(city: any = "") {
    let promise = new Promise((resolve, reject) => {
      this.cs.getData(city.value).subscribe((res: any) => {
        resolve(res)
      },err => {
        resolve(null)
      })
    })

    return promise
  }

  scrollHandler(data: any) {
    console.log(data)

  }


}
