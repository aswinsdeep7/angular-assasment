import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private apiService: HttpClient) { }
  selectedCountry = new Subject();
  selectedCountry$ = this.selectedCountry.asObservable();

  setselectedCountry(data:any){
    this.selectedCountry.next(data);
  } 
  
  getData(city:string) {
    return this.apiService.get('https://weather-by-api-ninjas.p.rapidapi.com/v1/weather', {
      headers: {
        'X-RapidAPI-Key': '9afc3ec4afmsh5915a5d0a5bf542p121f8djsnfbcef75dcad9',
        'X-RapidAPI-Host': 'weather-by-api-ninjas.p.rapidapi.com'
      },
      params: {city: `${city}`},
      responseType: 'text'
    })
  }

  getCityByCountryCode(country:any) {
    return this.apiService.get('https://referential.p.rapidapi.com/v1/city', {
      headers: {
        'X-RapidAPI-Key': '9afc3ec4afmsh5915a5d0a5bf542p121f8djsnfbcef75dcad9',
        'X-RapidAPI-Host': 'referential.p.rapidapi.com'
      },
      params: {
        fields: 'iso_a2,state_code,state_hasc,timezone,timezone_offset',
        iso_a2: `${country.code}`,
        lang: 'en',
        limit: '30'
      },
    })
  }
}
